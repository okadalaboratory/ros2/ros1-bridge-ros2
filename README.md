# ROS1とROS2のbridgeを試す

**これを読む！！！**

https://answers.ros.org/question/358103/ros-1-bridge-requires-the-topic-to-be-published-both-sides/


[Bridge communication between ROS 1 and ROS 2](https://github.com/ros2/ros1_bridge)

[osrf/ros:foxy-ros1-bridge](https://hub.docker.com/layers/osrf/ros/foxy-ros1-bridge/images/sha256-6eef6a0f2db151d8eba7233643fe468e74b1afa14b5ee01d5cfbc3db0d93a181?context=explore)

[ROS複数Docker通信](https://qiita.com/tomoyafujita/items/07937a25bc48aa076056)

[ros1_bridgeの導入](https://tomson784.github.io/ros_practice/ROS2/bridge_setup.html)

[ROS1 Noetic とROS2 FoxyでUUV Simulatorを使う](https://tshell.hatenablog.com/entry/2022/07/16/212407)

osrf/rockerを使ってPC内に複数のROS distroの開発環境を整える方法
https://memoteki.net/archives/4471#index_id2



ROSで
```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
After that you should be able to install the ros1-bridge deb:

sudo apt install ros-foxy-ros1-bridge
```
roscore

docker run --rm  --privileged --net=host --ipc=host  -it osrf/ros:foxy-ros1-bridge bash
ros2 run ros1_bridge dynamic_bridge --bridge-all-topics






alias docker_show_all='docker ps -a -q' # Shows all the containers
alias docker_delete_all='docker rm $(docker ps -a -q)' # Deletes all Containers
alias docker_start='docker start -i ros2Docker' # Starts our default container
alias docker_term='docker exec -it ros2Docker bash' # To run another terminal in the same container
